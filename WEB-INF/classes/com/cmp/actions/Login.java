package com.cmp.actions;

import com.opensymphony.xwork2.ActionSupport;

public class Login extends ActionSupport{

      private static final long serialVersionUID = 1L;
      private String saludo;
     
      public String execute() throws Exception {
            setSaludo("Please Sign In");
            return "SUCCESS";
      }
      public String getSaludo() {
            return saludo;
      }
      public void setSaludo(String saludo) {
            this.saludo = saludo;
      }

}